﻿using UnityEngine;
using UnityEngine.UI;

public class Birthdate : MonoBehaviour
{
    [SerializeField]
    private DayContainer _dayContainer;

    [SerializeField]
    private MonthContainer _monthContainer;

    [SerializeField]
    Button btnValidate;

    private string _month;

    private string _day;

    private const string _FEBRUARY = "February";

    private const int _LAST_DAY_FEBRUARY = 29;

    private void Start()
    {
        btnValidate.onClick.AddListener(ValidateBirthdate);
    }

    private void ValidateBirthdate()
    {
        _month = _monthContainer.GetMonth();

        _day = _dayContainer.GetDay();

        if (_month.Equals(_FEBRUARY))
        {
            if (int.Parse(_day) > _LAST_DAY_FEBRUARY)
            {
                _day = _LAST_DAY_FEBRUARY.ToString();
            }
        }

        Debug.Log("Birthdate selected is : " + _day + " " + _month);
    }
}
