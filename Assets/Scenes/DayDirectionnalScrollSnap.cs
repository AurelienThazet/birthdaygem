﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DayDirectionnalScrollSnap : ScrollSnaps.DirectionalScrollSnap
{
    private string _daySelected = "";

    protected override void UpdateSnapPositions()
    {
        base.UpdateSnapPositions();

        _daySelected = closestItem.name;
    }

    public string GetDaySelected()
    {
        return _daySelected;
    }
}
