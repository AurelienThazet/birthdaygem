﻿using System.Globalization;
using UnityEngine;

public class MonthContainer : MonoBehaviour
{
    [SerializeField]
    private AddMonths _addMonths;

    [SerializeField]
    private MonthDirectionnalScrollSnap _monthDirectionnalScrollSnap;

    private string[] monthsNames = DateTimeFormatInfo.CurrentInfo.MonthNames;

    private const int _ITEM_MARGIN = 200;

    private void Start()
    {
        SetAllMonths();
    }

    private void SetAllMonths()
    {
        // Using monthNames.Length-1 because table containing one empty element
        for (int i = 0; i < monthsNames.Length - 1; i++)
        {
            _addMonths.SetMonth(monthsNames[i], _ITEM_MARGIN);

            _addMonths.Add();
        }
    }

    public string GetMonth()
    {
        return _monthDirectionnalScrollSnap.GetMonthSelected();
    }
}
