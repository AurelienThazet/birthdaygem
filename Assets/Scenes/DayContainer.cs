﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DayContainer : MonoBehaviour
{
    [SerializeField]
    private AddDays _addDays;

    [SerializeField]
    private DayDirectionnalScrollSnap _dayDirectionnalScrollSnap;

    private const int _NB_DAYS = 31;

    private const int _ITEM_MARGIN = 400;

    private void Start()
    {
        SetAllDays();
    }

    private void SetAllDays()
    {
        for (int i = 0; i < _NB_DAYS; i++)
        {
            _addDays.SetDay(i + 1, _ITEM_MARGIN);

            _addDays.Add();
        }
    }

    public string GetDay()
    {
        return _dayDirectionnalScrollSnap.GetDaySelected();
    }
}
