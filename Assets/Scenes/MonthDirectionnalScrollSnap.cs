﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class MonthDirectionnalScrollSnap : ScrollSnaps.DirectionalScrollSnap
{
    private string _monthSelected = "";

    protected override void UpdateSnapPositions()
    {
        base.UpdateSnapPositions();

        _monthSelected = closestItem.name;
    }

    public string GetMonthSelected()
    {
        return _monthSelected;
    }
}
